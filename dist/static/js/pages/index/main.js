global.webpackJsonp([3],{

/***/ 102:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vue__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_vue__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__index__ = __webpack_require__(103);



var app = new __WEBPACK_IMPORTED_MODULE_0_vue___default.a(__WEBPACK_IMPORTED_MODULE_1__index__["a" /* default */]);
app.$mount();

/* harmony default export */ __webpack_exports__["default"] = ({
  config: {
    navigationBarTitleText: '首页',
    usingComponents: {
      "van-button": "../../static/dist/button/index",
      "van-notice-bar": "../../static/dist/notice-bar/index",
      "van-slider": "../../static/dist/slider/index",
      "van-field": "../../static/dist/field/index",
      "van-nav-bar": "../../static/dist/nav-bar/index",
      "van-icon": "../../static/dist/icon/index",
      "van-tabbar": "../../static/dist/tabbar/index",
      "van-tabbar-item": "../../static/dist/tabbar-item/index"
    }
  }
});

/***/ }),

/***/ 103:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_mpvue_loader_lib_selector_type_script_index_0_index_vue__ = __webpack_require__(105);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__node_modules_mpvue_loader_lib_template_compiler_index_id_data_v_161afc18_hasScoped_true_transformToRequire_video_src_source_src_img_src_image_xlink_href_node_modules_mpvue_loader_lib_selector_type_template_index_0_index_vue__ = __webpack_require__(109);
var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(104)
}
var normalizeComponent = __webpack_require__(4)
/* script */

/* template */

/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-161afc18"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_mpvue_loader_lib_selector_type_script_index_0_index_vue__["a" /* default */],
  __WEBPACK_IMPORTED_MODULE_1__node_modules_mpvue_loader_lib_template_compiler_index_id_data_v_161afc18_hasScoped_true_transformToRequire_video_src_source_src_img_src_image_xlink_href_node_modules_mpvue_loader_lib_selector_type_template_index_0_index_vue__["a" /* default */],
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "src/pages/index/index.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {return key !== "default" && key.substr(0, 2) !== "__"})) {console.error("named exports are not supported in *.vue files.")}
if (Component.options.functional) {console.error("[vue-loader] index.vue: functional components are not supported with templates, they should use render functions.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-161afc18", Component.options)
  } else {
    hotAPI.reload("data-v-161afc18", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

/* harmony default export */ __webpack_exports__["a"] = (Component.exports);


/***/ }),

/***/ 104:
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ 105:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__components_card__ = __webpack_require__(44);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__components_myTabBar__ = __webpack_require__(43);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

// 组件引用



/* harmony default export */ __webpack_exports__["a"] = ({
  data: function data() {
    return {
      motto: 'Hello World',
      readonly: false,
      demo1: '',
      menus: {
        menu1: 'Take Photo',
        menu2: 'Choose from photos'
      },
      showMenus: false,
      list: ['a', 'b', 'c'],
      result: ['a', 'b'],
      sms: ''
    };
  },

  components: {
    card: __WEBPACK_IMPORTED_MODULE_0__components_card__["a" /* default */],
    myTabBar: __WEBPACK_IMPORTED_MODULE_1__components_myTabBar__["a" /* default */]
  },
  methods: {
    gotoGame: function gotoGame(path) {
      this.navigatePageTo(this.router + path + '?id=123456');
    }
  }
});

/***/ }),

/***/ 109:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;
  return _c('div', {
    staticClass: "container"
  }, [_c('div', {
    staticStyle: {
      "width": "100%",
      "text-align": "left",
      "padding-bottom": "50px"
    }
  }, [_c('van-nav-bar', {
    attrs: {
      "title": "标题",
      "left-text": "返回",
      "left-arrow": "",
      "mpcomid": '1'
    }
  }, [_c('van-icon', {
    attrs: {
      "name": "search",
      "mpcomid": '0'
    },
    slot: "right"
  })], 1), _vm._v(" "), _c('van-button', {
    attrs: {
      "type": "primary",
      "mpcomid": '2'
    }
  }, [_vm._v("按钮")]), _vm._v(" "), _c('van-notice-bar', {
    attrs: {
      "left-icon": "https://img.yzcdn.cn/1.png",
      "text": "足协杯战线连续第2年上演广州德比战，上赛季半决赛上恒大以两回合5-3的总比分淘汰富力。",
      "mpcomid": '3'
    }
  }), _vm._v(" "), _c('div', {
    staticStyle: {
      "padding": "20px 0"
    }
  }, [_c('van-cell-group', {
    attrs: {
      "mpcomid": '6'
    }
  }, [_c('van-field', {
    attrs: {
      "center": "",
      "clearable": "",
      "label": "短信验证码",
      "placeholder": "请输入短信验证码",
      "use-button-slot": "",
      "eventid": '0',
      "mpcomid": '5'
    },
    model: {
      value: (_vm.sms),
      callback: function($$v) {
        _vm.sms = $$v
      },
      expression: "sms"
    }
  }, [_c('van-button', {
    attrs: {
      "size": "small",
      "type": "primary",
      "mpcomid": '4'
    },
    slot: "button"
  }, [_vm._v("发送验证码")])], 1)], 1)], 1), _vm._v(" "), _c('img', {
    staticClass: "girl",
    attrs: {
      "src": _vm.imgSrc + 'static/img/girl.png',
      "alt": ""
    }
  }), _vm._v(" "), _c('img', {
    staticClass: "logo",
    attrs: {
      "src": "../../assets/logo.png",
      "alt": ""
    }
  }), _vm._v(" "), _c('card', {
    attrs: {
      "text": _vm.motto,
      "mpcomid": '7'
    }
  }), _vm._v(" "), _c('form', {
    staticClass: "form-container"
  }, [_c('input', {
    directives: [{
      name: "model",
      rawName: "v-model",
      value: (_vm.motto),
      expression: "motto"
    }],
    staticClass: "form-control",
    attrs: {
      "type": "text",
      "placeholder": "v-model",
      "eventid": '1'
    },
    domProps: {
      "value": (_vm.motto)
    },
    on: {
      "input": function($event) {
        if ($event.target.composing) { return; }
        _vm.motto = $event.target.value
      }
    }
  }), _vm._v(" "), _c('input', {
    directives: [{
      name: "model",
      rawName: "v-model.lazy",
      value: (_vm.motto),
      expression: "motto",
      modifiers: {
        "lazy": true
      }
    }],
    staticClass: "form-control",
    attrs: {
      "type": "text",
      "placeholder": "v-model.lazy",
      "eventid": '2'
    },
    domProps: {
      "value": (_vm.motto)
    },
    on: {
      "change": function($event) {
        _vm.motto = $event.target.value
      }
    }
  })]), _vm._v(" "), _c('a', {
    staticClass: "counter",
    attrs: {
      "eventid": '3'
    },
    on: {
      "click": function($event) {
        _vm.gotoGame('pages/counter/main')
      }
    }
  }, [_vm._v("去往Vuex示例页面")]), _vm._v(" "), _c('a', {
    staticClass: "counter",
    attrs: {
      "eventid": '4'
    },
    on: {
      "click": function($event) {
        _vm.gotoGame('pages/logs/main')
      }
    }
  }, [_vm._v("去往logs页面")])], 1), _vm._v(" "), _c('div', [_c('my-tab-bar', {
    attrs: {
      "activeIndex": 0,
      "mpcomid": '8'
    }
  })], 1)])
}
var staticRenderFns = []
render._withStripped = true
var esExports = { render: render, staticRenderFns: staticRenderFns }
/* harmony default export */ __webpack_exports__["a"] = (esExports);
if (false) {
  module.hot.accept()
  if (module.hot.data) {
     require("vue-hot-reload-api").rerender("data-v-161afc18", esExports)
  }
}

/***/ })

},[102]);
//# sourceMappingURL=main.js.map